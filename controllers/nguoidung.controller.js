const pool = require("../pooldb");

module.exports.index = (req, res) => {
    if (req.isAuthenticated()) {
        let query = "SELECT * FROM nguoidung ORDER BY id ASC";
        pool.connect((err, client, done) => {
            if (err) {
                return console.error("error fetching client from pool", err);
            }
            client.query(query, (err, result) => {
                done();
                if (err) {
                    res.end();
                    return console.error("error running query", err);
                }
                res.render("nguoidung/nguoidung_list", { danhsach: result });
            });
        });
    } else {
        res.send("Chua dang nhap");
    }
};

module.exports.them = (req, res) => {
    if (req.isAuthenticated()) {
        res.render("nguoidung/nguoidung_insert");
    } else {
        res.send("Chua dang nhap");
    }
};

module.exports.postThem = (req, res) => {
    if (req.isAuthenticated()) {
        let hoten = req.body.txtHoten;
        let email = req.body.txtEmail;
        let query = "INSERT INTO nguoidung(hoten, email) VALUES ('" + hoten + "', '" + email + "');";
        pool.connect((err, client, done) => {
            if (err) {
                return console.error("error fetching client from pool", err);
            }
            client.query(query, (err, result) => {
                done();
                if (err) {
                    res.end();
                    return console.error("error running query", err);
                }
                res.redirect("../nguoidung");
            });
        });
    } else {
        res.send("Chua dang nhap");
    }
};

module.exports.sua = (req, res) => {
    if (req.isAuthenticated()) {
        let id = req.params.id;
        let query = "SELECT * FROM nguoidung WHERE id='" + id + "';";
        pool.connect((err, client, done) => {
            if (err) {
                return console.error("error fetching client from pool", err);
            }
            client.query(query, (err, result) => {
                done();
                if (err) {
                    res.end();
                    return console.error("error running query", err);
                }
                res.render("nguoidung/nguoidung_edit", { nd: result.rows[0] });
            });
        });
    } else {
        res.send("Chua dang nhap");
    }
};
module.exports.postSua = (req, res) => {
    if (req.isAuthenticated()) {
        let id = req.body.txtId;
        let hoten = req.body.txtHoten;
        let email = req.body.txtEmail;
        let query = "UPDATE nguoidung SET hoten='" + hoten + "', email='" + email + "' WHERE id='" + id + "';";
        pool.connect((err, client, done) => {
            if (err) {
                return console.error("error fetching client from pool", err);
            }
            client.query(query, (err, result) => {
                done();
                if (err) {
                    res.end();
                    return console.error("error running query", err);
                }
                res.redirect("../nguoidung");
            });
        });
    } else {
        res.send("Chua dang nhap");
    }
};

module.exports.xoa = (req, res) => {
    if (req.isAuthenticated()) {
        let id = req.params.id;
        let query = "DELETE FROM nguoidung WHERE id='" + id + "';";
        pool.connect((err, client, done) => {
            if (err) {
                return console.error("error fetching client from pool", err);
            }
            client.query(query, (err, result) => {
                done();
                if (err) {
                    res.end();
                    return console.error("error running query", err);
                }
                res.redirect("../../nguoidung");
            });
        });
    } else {
        res.send("Chua dang nhap");
    }
};