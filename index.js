const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");
const passport = require("passport");
const localStrategy = require("passport-local").Strategy;

const pool = require("./pooldb");
const nguoiDungRoute = require("./routes/nguoidung.route");

const app = express();

const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log("Server listening on port " + port);
});

app.use(express.static("public"));
app.set("view engine", "ejs");
app.set("views", "./views");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(session({
    secret: "mysecret",
    cookie: {
        maxAge: 60000 * 10
    },
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

//passport
passport.use(new localStrategy((username, password, done) => {
    let query = "SELECT * FROM admin WHERE username='" + username + "' AND password='" + password + "';";
    pool.connect((err, client, done2) => {
        if (err) {
            return console.error("error fetching client from pool", err);
        }
        client.query(query, (err, result) => {
            done2();
            if (err) {
                return console.error("error running query", err);
            }
            if (result.rowCount != 0) {
                dung(result.rows[0].username, true);
            } else {
                dung(null, false);
            }
        });
    });
    function dung(username, check) {
        if (check) {
            done(null, username);
        } else {
            done(null, false);
        }
    }
}));
passport.serializeUser((user, done) => {
    done(null, user);
});
passport.deserializeUser((name, done) => {
    if (name) {
        return done(null, name);
    } else {
        return done(null, false);
    }
});

app.get("/", (req, res) => {
    res.render("main");
})

app.route("/login")
    .get((req, res) => {
        res.render("login");
    })
    .post(passport.authenticate("local", { failureRedirect: "/login", successRedirect: "/nguoidung" }));

app.get("/logout", (req, res) => {
    req.logOut();
    req.session.destroy((err) => {
        res.redirect('/');
    });
});

app.use("/nguoidung", nguoiDungRoute);
