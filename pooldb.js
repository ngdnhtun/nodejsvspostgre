const pg = require("pg");
const config = {
    user: "postgres",
    database: "QuanLy",
    password: "123",
    host: "localhost",
    port: 5432,
    max: 10,
    idleTimeoutMillis: 30000,
};
const pool = new pg.Pool(config);

module.exports = pool;