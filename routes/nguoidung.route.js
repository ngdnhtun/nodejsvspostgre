const express = require("express");

const controller = require("../controllers/nguoidung.controller");

const router = express.Router();

router.get("/", controller.index);

router.route("/them")
    .get(controller.them)
    .post(controller.postThem);

router.get("/sua/:id", controller.sua);
router.post("/sua", controller.postSua);

router.get("/xoa/:id", controller.xoa);

module.exports = router;